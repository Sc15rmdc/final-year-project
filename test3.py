fileInput = open("score.txt", "r")
array = []
average = 0
low = 0
high = 0
count = 0
count1 = 0
count2 = 0

for i in fileInput:
    array.append(i.split(','))

for i in array:
    lowest = 100
    highest = 0
    for j in i:
        if int(j) < lowest:
            lowest = int(j)
        if int(j) > highest:
            highest = int(j)
        average += int(j)
        count += 1
    low += lowest
    high += highest
    count1 += 1

average = average/count
low = low/(count/7)
high = high/(count/7)

print("lowest: {0}".format(low))
print("average: {0}".format(average))
print("highest: {0}".format(high))
