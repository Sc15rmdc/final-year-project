# function that update the value of the card after been built
def Update(player, card):
    # update brown card
    if card[1] == "brown":
        UpdateBrownCard(player, card)
    # update grey card
    elif card[1] == "grey":
        UpdateGreyCard(player, card)
    # update purple card
    elif card[1] == "purple":
        UpdatePurpleCard(player, card)
    # update blue card
    elif card[1] == "blue":
        UpdateBlueCard(player, card)
    # update yellow card
    elif card[1] == "yellow":
        UpdateYellowCard(player, card)
    # update green card
    elif card[1] == "green":
        UpdateGreenCard(player, card)
    # update red card
    elif card[1] == "red":
        UpdateRedCard(player, card)


# function that update the brown card
def UpdateBrownCard(player, card):
    player.card.append(card[0])
    player.cardNum[0] += 1
    if card[3].count("WoodBrick") == 1:
        player.coin -= 1
        player.supplySet.append(["wood", "brick"])
    elif card[3].count("WoodOre") == 1:
        player.coin -= 1
        player.supplySet.append(["wood", "ore"])
    elif card[3].count("StoneWood") == 1:
        player.coin -= 1
        player.supplySet.append(["stone", "wood"])
    elif card[3].count("StoneBrick") == 1:
        player.coin -= 1
        player.supplySet.append(["stone", "brick"])
    elif card[3].count("BrickOre") == 1:
        player.coin -= 1
        player.supplySet.append(["brick", "ore"])
    elif card[3].count("OreStone") == 1:
        player.coin -= 1
        player.supplySet.append(["ore", "stone"])
    else:
        if card[2] == 1:
            player.coin -= 1
        for i in card[3]:
            player.supplySet.append([i])


# function that update the grey card
def UpdateGreyCard(player, card):
    player.card.append(card[0])
    player.cardNum[1] += 1
    for i in card[2]:
        player.supplySet.append([i])


# function that update the purple card
def UpdatePurpleCard(player, card):
    player.card.append(card[0])
    player.cardNum[2] += 1
    # if card not have effect that mean it provide wildTech
    if card[3] is None:
        player.pEffect.append(card)
    else:
        player.pEffect.append(card[3])


# function that update the blue card
def UpdateBlueCard(player, card):
    player.card.append(card[0])
    player.cardNum[3] += 1
    player.point[3] += card[3]
    if card[4] is not None:
        player.chains.append(card[4])


# function that update the yellow card
def UpdateYellowCard(player, card):
    player.card.append(card[0])
    player.cardNum[4] += 1
    if card[3] is not None:
        player.coin += card[3]
    if card[4] is not None:
        if card[4] == "wildBrown":
            player.wonderSet.append(["stone", "brick", "ore", "wood"])
        elif card[4] == "wildGrey":
            player.wonderSet.append(["paper", "cloth", "glass"])
    if card[5] is not None:
        if card[5] == "Vineyard":
            player.coin += player.cardNum[0] + player.neighbour[0].cardNum[0] + player.neighbour[1].cardNum[0]
        elif card[5] == "Bazar":
            player.coin += 2*(player.cardNum[1] + player.neighbour[0].cardNum[1] + player.neighbour[1].cardNum[1])
        elif card[5] == "Haven":
            player.yEffect.append(card[5])
            player.coin += player.cardNum[0]
        elif card[5] == "LightHouse":
            player.yEffect.append(card[5])
            player.coin += player.cardNum[4]
        elif card[5] == "Chamber of Commerce":
            player.yEffect.append(card[5])
            player.coin += 2*player.cardNum[1]
        elif card[5] == "Arena":
            player.yEffect.append(card[5])
            player.coin += 3*player.stage
    if card[6] is not None:
        player.cheapTradeLeft += card[6].count("cheapTradeLeft")
        player.cheapTradeRight += card[6].count("cheapTradeRight")
        player.cheapTradeBoth[1] += card[6].count("cheapTradeBothGrey")
    if card[7] is not None:
        player.chains.append(card[7])


# function that update the red card
def UpdateRedCard(player, card):
    player.card.append(card[0])
    player.cardNum[5] += 1
    player.shield += card[3]
    if card[4] is not None:
        player.chains.append(card[4])


# function that update the green card
def UpdateGreenCard(player, card):
    player.card.append(card[0])
    player.cardNum[6] += 1
    player.symbol[0] += card[3].count("gear")
    player.symbol[1] += card[3].count("compass")
    player.symbol[2] += card[3].count("tablet")
    if card[4] is not None:
        for i in card[4]:
            player.chains.append(i)


# # function that update the value of the wonder after been built
def UpdateWonder(player):
    # adding stage 1 reward to player
    if player.stage == 0:
        player.stage += 1
        for i in range(0, len(player.wonder[4])):
            if player.wonder[4][i][0] == "point":
                player.point[2] += player.wonder[4][i][1]
            elif player.wonder[4][i][0] == "shield":
                player.shield += player.wonder[4][i][1]
            elif player.wonder[4][i][0] == "coin":
                player.coin += player.wonder[4][i][1]
            elif player.wonder[4][i][0] == "buildDiscardForFree":
                player.buildDiscardForFree += 1
            elif player.wonder[4][i] == "wildBrown":
                player.wonderSet.append(["wood", "brick", "stone", "ore"])
            elif player.wonder[4][i] == "cheapTradeBothBrown":
                player.cheapTradeBoth[0] += 1
    # adding stage 2 reward to player
    elif player.stage == 1:
        player.stage += 1
        for i in range(0, len(player.wonder[6])):
            if player.wonder[6][i][0] == "point":
                player.point[2] += player.wonder[6][i][1]
            elif player.wonder[6][i][0] == "shield":
                player.shield += player.wonder[6][i][1]
            elif player.wonder[6][i][0] == "coin":
                player.coin += player.wonder[6][i][1]
            elif player.wonder[6][i][0] == "buildDiscardForFree":
                player.buildDiscardForFree += 1
            elif player.wonder[6][i] == "buildDiscardForFree":
                player.buildDiscardForFree += 1
            elif player.wonder[6][i] == "wildBrown":
                player.wonderSet.append(["wood", "brick", "stone", "ore"])
            elif player.wonder[6][i] == "cheapTradeBothBrown":
                player.cheapTradeBoth[0] += 1
            elif player.wonder[6][i] == "wildTech":
                player.wildTech += 1
            elif player.wonder[6][i] == "buildForFree":
                player.buildForFree[0] = True
            elif player.wonder[6][i] == "wildGrey":
                player.wonderSet.append(["glass", "cloth", "paper"])
            elif player.wonder[6][i] == "buildLastForFree":
                player.buildLastForFree = True
    # adding stage 3 reward to player
    elif player.stage == 2:
        player.stage += 1
        for i in range(0, len(player.wonder[8])):
            if player.wonder[8][i][0] == "point":
                player.point[2] += player.wonder[8][i][1]
            elif player.wonder[8][i][0] == "shield":
                player.shield += player.wonder[8][i][1]
            elif player.wonder[8][i][0] == "coin":
                player.coin += player.wonder[8][i][1]
            elif player.wonder[8][i] == "wildTech":
                player.wildTech += 1
            elif player.wonder[8][i] == "copy":
                player.copy = True
            elif player.wonder[8][i] == "buildDiscardForFree":
                player.buildDiscardForFree += 1
    # adding stage 4 reward to player
    elif player.stage == 3:
        player.stage += 1
        if player.wonder[10][0][0] == "point":
            player.point[2] += player.wonder[10][0][1]
