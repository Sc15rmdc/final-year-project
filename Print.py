# function that convert the card array into text so the player can understand what the card do.=
def CardPrint(deck):
    if deck[1] == 'brown':
        if deck[2] == 0:
            return "{0} - It will provides {1}".format(deck[0], deck[3])
        else:
            return "{0} - It costs coin: {1}, It will provides {2}".format(deck[0], deck[2], deck[3])
    elif deck[1] == 'grey':
        return "{0} - It will provides {1}".format(deck[0], deck[2])
    elif deck[1] == 'purple':
        if deck[0] == 'Workers Guild':
            return "{0} - It costs resource: {1} and it give 1 VP per brown card your neighbour got".format(deck[0], deck[2])
        elif deck[0] == 'Craftsmens Guild':
            return "{0} - It costs resource: {1} and it give 2 VP per grey card your neighbour got".format(deck[0], deck[2])
        elif deck[0] == 'Traders Guild':
            return "{0} - It costs resource: {1} and it give 1 VP per yellow card your neighbour got".format(deck[0], deck[2])
        elif deck[0] == 'Philosophers Guild':
            return "{0} - It costs resource: {1} and it give 1 VP per green card your neighbour got".format(deck[0], deck[2])
        elif deck[0] == 'Spies Guild':
            return "{0} - It costs resource: {1} and it give 1 VP per red card your neighbour got".format(deck[0], deck[2])
        elif deck[0] == 'Strategists Guild':
            return "{0} - It costs resource: {1} and it give 1 VP per defeat token your neighbour got".format(deck[0], deck[2])
        elif deck[0] == 'Shipowners Guild':
            return "{0} - It costs resource: {1} and it give 1 VP per brown card, grey card or purple card you got".format(deck[0], deck[2])
        elif deck[0] == 'Scientists Guild':
            return "{0} - It costs resource: {1} and it give a compass or gear or tablet".format(deck[0], deck[2])
        elif deck[0] == 'Magistrates Guild':
            return "{0} - It costs resource: {1} and it give 1 VP per blue card your neighbour got".format(deck[0], deck[2])
        elif deck[0] == 'Builders Guild':
            return "{0} - It costs resource: {1} and it give 1 VP per stage of wonder you and your neighbour built".format(deck[0], deck[2])
    elif deck[1] == 'blue':
        if deck[2] is None and deck[4] is not None:
            return "{0} - It will provides VP: {1} and It can chain to {2}".format(deck[0], deck[3], deck[4])
        elif deck[2] is None and deck[4] is None:
            return "{0} - It will provides VP: {1}".format(deck[0], deck[3])
        else:
            return "{0} - It costs {1}, It will provides VP: {2} and It can chain to {3}".format(deck[0], deck[2], deck[3], deck[4])
    elif deck[1] == 'yellow':
        if deck[0] == 'Vineyard':
            return "{0} - it give 1 gold per you and neighbour brown card got".format(deck[0])
        elif deck[0] == 'Bazar':
            return "{0} - it give 2 gold per you and neighbour grey card got".format(deck[0])
        elif deck[0] == 'Tavern':
            return "{0} - it give 5 gold".format(deck[0])
        elif deck[0] == 'Haven':
            return "{0} - it costs {1} and it give 1 gold and 1 VP per brown card you got".format(deck[0], deck[2])
        elif deck[0] == 'LightHouse':
            return "{0} - it costs {1} and it give 1 gold and 1 VP per yellow card you got".format(deck[0], deck[2])
        elif deck[0] == 'Chamber of Commerce':
            return "{0} - it costs {1} and it give 2 gold and 2 VP per grey card you got".format(deck[0], deck[2])
        elif deck[0] == 'Arena':
            return "{0} - it cost {1} and it give 3 gold and 1 VP per wonder stage you got".format(deck[0], deck[2])
        else:
            if deck[2] is None:
                if deck[3] is None:
                    return "{0} - It will provides cheapTrade: {1} It can chain to {2}".format(deck[0], deck[6], deck[7])
            return "{0} - It costs {1} resource: {2} It can chain to {3}".format(deck[0], deck[2], deck[4], deck[7])
    elif deck[1] == 'red':
        if deck[4] is None:
            return "{0} - It costs {1}, It will provides shield: {2}".format(deck[0], deck[2], deck[3])
        return "{0} - It costs {1}, It will provides shield: {2} and It can chain to {3}".format(deck[0], deck[2], deck[3], deck[4])
    elif deck[1] == 'green':
        if deck[4] is None:
            return "{0} - It costs {1}, It will provides a {2}".format(deck[0], deck[2], deck[3])
        return "{0} - It costs {1}, It will provides a {2} and It can chain to {3}".format(deck[0], deck[2], deck[3], deck[4])
