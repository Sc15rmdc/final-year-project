import copy


def sufficient_resources(needs, supply_sets):
    got = copy.copy(needs)
    sets = copy.copy(supply_sets)
    for i in needs:
        for j in sets:
            count = 0
            temp = []
            if len(j) is 1:
                if i in j:
                    got.remove(i)
                    sets.remove(j)
                    break
            else:
                for k in j:
                    if k in needs:
                        if k not in temp:
                            count += 1
                            temp.append(k)
                if count is 1:
                    got.remove(i)
                    sets.remove(j)
                    break
    c = copy.copy(got)
    for i in c:
        for j in sets:
            if i in j:
                got.remove(i)
                sets.remove(j)
                break
    if len(got) == 0:
        return True
    else:
        return got




tests = [(["brick", "stone", "brick", "wood"], [["glass"], ["stone", "ore"], ["brick", "wood"], ["wood", "ore", "brick", "stone"], ["cloth"]]),
         ([1, 1, 2, 2, 3], [[1, 3], [1, 2], [1, 2], [1], [1]]),
         ([1, 1, 1, 2, 2, 3], [[1, 3], [1, 2], [1, 2], [1], [1]]),
         ([1, 3, 5, 6], [[1, 2], [3, 4], [5, 6], [2, 5]])]

for test in tests:
    print("needs =", test[0])
    print("supply_sets =", test[1])
    result = sufficient_resources(test[0], test[1])
    print("sufficient_resources?", result)
