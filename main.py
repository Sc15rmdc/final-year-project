from Action import *
import copy
from random import randint

# create instance of the class
from Deck import Deck
from Player import Player
from Result import CalculatePoint
from UpdateCard import UpdateWonder
from Wonder import Wonder
from AI import ChooseAIMove

output = open("score.txt", 'w')

# main function that run the game
def main():
    deck = Deck()
    wonder = Wonder()

    listOfPlayer = []
    # print("Please enter the number of player? min = 3 and max = 7: ")
    # # how many player playing the game
    # numOfPlayer = input(">>> ")
    numOfPlayer = 7
    # validate user input
    while not 2 < int(numOfPlayer) < 8:
        print("Please enter a integer and it's between 3 and 7: ")
        numOfPlayer = input(">>> ")
    # create player and add to list
    for i in range(0, int(numOfPlayer)):
        player = Player()
        # change the player name
        player.name = "AI{0}".format(i)
        # if i == 0:
        #     player.name = "Your"
        # locate wonder for the player
        player.wonder = wonder.ChooseWonder()
        # update initial resource to the player
        player.supplySet.append([player.wonder[1]])
        # add player into the list
        listOfPlayer.append(player)
    # add neighbour to the list
    UpdateNeighbour(listOfPlayer)
    # loop age up to 3
    for age in range(1, 4):
        print("="*150)
        print("Age {0}".format(age))
        # add age card to the player hand
        deck.DeckToPlayerHand(age, listOfPlayer)
        # loop turn up to 7
        for turn in range(1, 8):
            print("="*150)
            print("Turn {0}:".format(turn))
            # loop player up to the number of player
            for player in listOfPlayer:
                if turn is not 7:
                    print("="*150)
                    print("It is {0}'s turn:".format(player.name))
                    print("="*150)
                    # ask the user to choose a card to play
                    if player.name == "Your":
                    # ask the player to see whether they want to see the neighbour detail
                        print("Want to see the all user detail?\n1. Yes\n2. No")
                        userInput = input(">>> ")
                        if userInput == 1:
                            PlayerDetail(listOfPlayer)
                        ChooseCard(player)
                    else:
                        ChooseAIMove(player, False)
                else:
                    # check if the player can play the last card
                    # need to change
                    if player.buildLast is True:
                        if player.name == "Your":
                            print("{0}".format(CardPrint(player.hand)))
                            card = player.hand.pop(0)
                            print("\n1. Build the card\n\n2. Build wonder\n\n3. sell the card\n\nPlease enter the number of action: ")
                            choice1 = input(">>> ")
                            while 1:
                                if choice1 == 1:
                                    if Build(player, i, False) is True:
                                        Update(player, card)
                                    elif Build(player, i, True) is True:
                                        test = Build(player, card[0], True)
                                        player.coin -= test[0]
                                        player.neighbour[0].coin += test[0]
                                        player.coin -= test[1]
                                        player.neighbour[1].coin += test[1]
                                        Update(player, card)
                                    else:
                                        continue
                                elif choice1 == 2:
                                    if BuildWonder(player) is True:
                                        UpdateWonder(player)
                                        print("\nYou have built the wonder")
                                    else:
                                        print("\nYou do not have enough resource to build the wonder")
                                elif choice1 == 3:
                                    Discard(player, card)
                        else:
                            ChooseAIMove(player, False)
                    else:
                        discard.append(player.hand[0])
                        player.hand.pop(0)
                # check if player can build discard for free
                buildDiscardForFree(player)
            for i in listOfPlayer:
                i.buildForFree[1] = False
            # swap the player hand depend on the age
            SwapPlayerHand(listOfPlayer, age)
        print("="*150)
        # show war result
        print("War:\n")
        # war at the end of the turn
        War(listOfPlayer, age)
    print("="*150)
    # show the end game result
    print("Result:\n")
    for player in listOfPlayer:
        print("{0}'s point is {1}".format(player.name, CalculatePoint(player)))
    output.write("{0}, {1}, {2}, {3}, {4}, {5}, {6}".format(listOfPlayer[0].fPoint, listOfPlayer[1].fPoint, listOfPlayer[2].fPoint, listOfPlayer[3].fPoint, listOfPlayer[4].fPoint, listOfPlayer[5].fPoint, listOfPlayer[6].fPoint))
    output.write("\n")

for i in range(0, 100):
    main()
