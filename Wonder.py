import random


class Wonder:
    def __init__(self):
        # A side board
        # A side board = [Name, resource, max Stage number, Stage resource cost, Stage reward [type, amount]]
        self.ARhodes = ["The Colossus of Rhodes A", "ore", 3, ["wood", "wood"], [["point", 3]], ["brick", "brick", "brick"],
                        [["shield", 2]], ["ore", "ore", "ore", "ore"], [["point", 7]]]

        self.AAlexandria = ["The Lighthouse of Alexandria A", "glass", 3, ["stone", "stone"], [["point", 3]], ["ore", "ore"],
                            ["wildBrown"], ["glass", "glass"], [["point", 7]]]

        self.AArtemis = ["The Temple of Artemis in Ephesus A", "paper", 3, ["stone", "stone"], [["point", 3]], ["wood", "wood"],
                         [["coin", 9]], ["paper", "paper"], [["point", 7]]]

        self.ABabylon = ["The Hanging Gardens of Babylon A", "brick", 3, ["brick", "brick"], [["point", 3]], ["wood", "wood", "wood"],
                         ["wildTech"], ["brick", "brick", "brick", "brick"], [["point", 7]]]

        self.AZeus = ["The Statue of Zeus in Olympia A", "wood", 3, ["wood", "wood"], [["point", 3]], ["stone", "stone"],
                      ["buildForFree"], ["ore", "ore"], [["point", 7]]]

        self.AHalicarnassus = ["The Mausoleum of Halicarnassus A", "cloth", 3, ["brick", "brick"], [["point", 3]],
                               ["ore", "ore", "ore"], ["buildDiscardForFree"], ["cloth", "cloth"], [["point", 7]]]

        self.AGiza = ["The Pyramids of Giza A", "stone", 3, ["stone", "stone"], [["point", 3]], ["wood", "wood", "wood"],
                      [["point", 5]], ["stone", "stone", "stone", "stone"], [["point", 7]]]

        # B side board
        # B side board = [Name, resource, max Stage number, Stage resource cost, Stage reward [type, amount]]
        self.BRhodes = ["The Colossus of Rhodes B", "ore", 2, ["stone", "stone", "stone"], [["shield", 1], ["point", 3],
                        ["coin", 3]], ["ore", "ore", "ore", "ore"], [["shield", 1], ["point", 4], ["coin", 4]]]

        self.BAlexandria = ["The Lighthouse of Alexandria B", "glass", 3, ["brick", "brick"], ["wildBrown"], ["wood", "wood"],
                            ["wildGrey"], ["stone", "stone", "stone"], [["point", 7]]]

        self.BArtemis = ["The Temple of Artemis in Ephesus B", "paper", 3, ["stone", "stone"], [["point", 2], ["coin", 4]],
                         ["wood", "wood"], [["point", 3], ["coin", 4]], ["paper", "cloth", "glass"], [["point", 5], ["coin", 4]]]

        self.BBabylon = ["The Hanging Gardens of Babylon B", "brick", 3, ["cloth", "brick"], [["point", 3]], ["glass", "wood", "wood"],
                         ["buildLastForFree"], ["brick", "brick", "brick", "paper"], ["wildTech"]]

        self.BZeus = ["The Statue of Zeus in Olympia B", "wood", 3, ["wood", "wood"], ["cheapTradeBothBrown"], ["stone", "stone"],
                      [["point", 5]], ["cloth", "Ore", "Ore"], ["copy"]]

        self.BHalicarnassus = ["The Mausoleum of Halicarnassus B", "cloth", 3, ["ore", "ore"], [["point", 2], ["buildDiscardForFree"]],
                               ["brick", "brick", "brick"], [["point", 1], ["buildDiscardForFree"]], ["glass", "paper", "cloth"],
                               ["buildDiscardForFree"]]

        self.BGiza = ["The Pyramids of Giza B", "stone", 4, ["wood", "wood"], [["point", 3]], ["stone", "stone", "stone"],
                      [["point", 5]], ["brick", "brick", "brick"], [["point", 5]], ["paper", "stone", "stone", "stone", "stone"],
                      [["point", 7]]]

        # All available Wonder
        self.wonder = [[self.ARhodes, self.BRhodes], [self.AAlexandria, self.BAlexandria], [self.AArtemis, self.BArtemis],
                       [self.ABabylon, self.BBabylon], [self.AZeus, self.BZeus], [self.AHalicarnassus, self.BHalicarnassus],
                       [self.AGiza, self.BGiza]]

    # Choose Wonder for player
    def ChooseWonder(self):
        # Random number between 0 to 6
        wNum = random.randint(0, len(self.wonder)-1)
        # return the chosen wonder and remove it from the list
        playerWonder = self.wonder.pop(wNum)
        # Random number between 0 and 1
        sNum = random.randint(0, 1)
        # return the side of the board
        return playerWonder[sNum]
