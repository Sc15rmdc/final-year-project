import copy
from random import randint

from Print import *
from UpdateCard import *
discard = []


# function that build the card
def Build(player, card, trade):
    # check whether the player already built the same card
    if player.card.count(card[0]) == 0:
        # check whether the player can build for free
        if player.chains.count(card[0]) == 0:
            # check whether the player can build
            if trade is False:
                if CheckBuild(player, card, None) is True:
                    return True
                else:
                    return False
            else:
                check = CheckBuild(player, card, None)
                if check is not True and check is not False:
                    test = Trade(player, check)
                    if test is not False:
                        return test
                    else:
                        return False
        else:
            return True
    else:
        return False


# function that build the wonder
def BuildWonder(player):
    # check whether the player have enough resource to build the wonder stage
    check = CheckBuild(player, None, player.wonder)
    if check is True:
        return [True]
    elif check is False:
        return [False]
    else:
        result = Trade(player, check)
        if result is False:
            return [False]
        else:
            return [True, result]


# function that check whether the player can build the card
def CheckBuild(player, card, wonder):
    # check the resource that card need to build
    check = CheckResource(player.stage, player.coin, card, wonder)
    # check if the player have all the resource to build (not counting the alternate resource)
    # yes
    if check is True:
        return True
    # no
    elif check is False:
        return False
    # maybe - need to check the alternate resource
    else:
        # work out if it possible to build the card
        result = sufficient_resources(check, player.supplySet+player.wonderSet)
        if result is True:
            return True
        return result


# function that check whether what resource it need to be the card if possible
def CheckResource(stage, coin, card, wonder):
    # check whether it check card or wonder
    if card is not None:
        # check the card colour
        # brown - either can be no cost or cost coin
        if card[1] == "brown":
            if card[2] <= coin:
                return True
            else:
                return False
        # grey - no cost
        elif card[1] == "grey":
            return True
        # other colour - need resource to build or no cost
        else:
            # need resource
            if card[2] is not None:
                return card[2]
            # no cost
            else:
                return True
    # wonder - always cost resource unless reach the max stage
    elif wonder is not None:
        # check whether the wonder is fully build
        if stage < wonder[2]:
            # get the index of the resource of the stage
            stage = (2*stage) + 3
            # return the resource it need to build the stage
            return wonder[stage]
        else:
            print("You reach the max stage")
            return False


# function that check whether the player have enough resource to build include alternate resource
def sufficient_resources(needs, supply_sets):
    got = copy.copy(needs)
    sets = copy.copy(supply_sets)
    for i in needs:
        for j in sets:
            for k in j:
                count = 0
                temp = []
                if len(j) == 1:
                    if i in k:
                        count += 1
                else:
                    if i in k:
                        if k not in temp:
                            count += 1
                            temp.append(k)
            if count == 1:
                got.remove(i)
                sets.remove(j)
                break
    c = copy.copy(got)
    for i in c:
        for j in sets:
            if i in j:
                got.remove(i)
                sets.remove(j)
                break
    if len(got) == 0:
        return True
    else:
        return got


# function that compute the cost of buying all the resource need from the neighbour
def Trade(player, missing):
    # copy variable so it does not effect the actual variable
    # neighbour resource set to check whether what resource should take if both resource in alternate resource is need
    neightbL = copy.copy(player.neighbour[0].supplySet)
    neightbR = copy.copy(player.neighbour[1].supplySet)
    # got is all the resource that need to build
    needs = copy.copy(missing)
    # cost need to pay to either Left neighbour or right neighbour
    costL = 0
    costR = 0
    for i in missing:
        count = 0
        if i is "brick" or i is "wood" or i is "stone" or i is "ore":
            for j in neightbR:
                for k in j:
                    if i in k:
                        if player.cheapTradeBoth[0] == 1 or player.cheapTradeRight == 1:
                            costR += 1
                        else:
                            costR += 2
                        needs.remove(i)
                        neightbR.remove(j)
                        count += 1
                        break
                if count == 1:
                    break
            if count == 0:
                for j in neightbL:
                    for k in j:
                        if i in k:
                            if player.cheapTradeBoth[0] == 1 or player.cheapTradeLeft == 1:
                                costL += 1
                            else:
                                costL += 2
                            needs.remove(i)
                            neightbL.remove(j)
                            count += 1
                            break
                    if count == 1:
                        break
        else:
            for j in neightbR:
                for k in j:
                    if i in k:
                        if player.cheapTradeBoth[1] == 1:
                            costR += 1
                        else:
                            costR += 2
                        needs.remove(i)
                        neightbR.remove(j)
                        count += 1
                        break
                if count == 1:
                    break
            if count == 0:
                for j in neightbL:
                    for k in j:
                        if i in k:
                            if player.cheapTradeBoth[1] == 1:
                                costL += 1
                            else:
                                costL += 2
                            needs.remove(i)
                            neightbL.remove(j)
                            count += 1
                            break
                    if count == 1:
                        break
    # check if all the need resource been bought
    if needs == []:
        total = costL + costR
        if player.coin >= total:
            return [costL, costR]
        else:
            return False
    else:
        return False


# function to build discard for free
def buildDiscardForFree(player):
    if player.buildDiscardForFree == 1:
        if player.name == 'Your':
            if discard is not []:
                print("="*150)
                print("Discard:\n")
                # show all the discard to the player to choose
                for i in range(0, len(discard)):
                    print('{0} - {1}'.format(i+1, CardPrint(discard[i])))
                # ask user to choose one
                print("Please enter the number of card you want to play: ")
                choice = input(">>> ")
                # remove the card from discard
                card = discard.pop(int(choice)-1)
                # add the card to player
                Update(player, card)
                player.buildDiscardForFree = 0
            else:
                print("No card in Discard")
                player.buildDiscardForFree = 0
        else:
            num = randint(0, len(discard)-1)
            card = discard.pop(num)
            Update(player, card)
            player.buildDiscardForFree = 0


# function that discard the card
def Discard(player, card):
    discard.append(card)
    player.coin += 3
    return True


# function that add player neighbour to the array [left, right]
def UpdateNeighbour(lists):
    for players in range(0, len(lists) - 1):
        lists[players].neighbour.append(lists[players - 1])
        lists[players].neighbour.append(lists[players + 1])
    lists[len(lists) - 1].neighbour.append(lists[len(lists) - 2])
    lists[len(lists) - 1].neighbour.append(lists[0])


# function that show the player detail
def PlayerDetail(lists):
    for player in lists:
        print("="*150)
        print("{0}'s cards contain {1}".format(player.name, player.card))
        print("{0} got {1} brown card, {2} grey card, {3} purple card, {4} blue card, {5} yellow card, {6} red card, {7} green card".format(player.name, player.cardNum[0], player.cardNum[1], player.cardNum[2], player.cardNum[3], player.cardNum[4], player.cardNum[5], player.cardNum[6]))
        print("{0} got final point: {1}, redPoint: {2}, coinPoint: {3}, wonderPoint: {4}, bluePoint: {5}, yellowPoint: {6}, purplePoint: {7}".format(player.name, player.fPoint, player.point[0], player.point[1], player.point[2], player.point[3], player.point[4], player.point[5], player.point[6]))
        print("{4} wonder is {0} - {3} - next stage will cost: {1} - reward: {2}".format(player.wonder[0], CheckWonderResource(player.stage, player.wonder), CheckWonderReward(player.stage, player.wonder), player.stage, player.name))
        print("{2} currently have coins: {0}, resource: {1}, resource cant trade: {3}".format(player.coin, player.supplySet, player.name, player.wonderSet))
        print("{0} got {1} shield, {2} age1 Victory token, {3} age2 Victory token, {4} age3 Victory token, {5} defeat token".format(player.name, player.shield, player.vToken[0], player.vToken[1], player.vToken[2], player.dToken))
        print("{0} got {1} gear, {2} compass, {3} tablet, {4} wildTech".format(player.name, player.symbol[0], player.symbol[1], player.symbol[2], player.wildTech))
        print("chains: {0}".format(player.chains))
        print("buildForFree: {0}".format(player.buildForFree))
        print("buildDiscardForFree: {0}".format(player.buildDiscardForFree))
        print("buildLast: {0}, copy: {1}".format(player.buildLast, player.copy))
        print("pEffect: {0}, yEffect: {1}".format(player.pEffect, player.yEffect))
        print("cheapTradeRight: {0}, cheapTradeLeft: {1}, cheapTradeBoth: {2}".format(player.cheapTradeRight, player.cheapTradeLeft, player.cheapTradeBoth))


# function that ask the player to choose card
def ChooseCard(player):
    print("="*150)
    # show player their current resource and stage
    print("Your wonder is {0} - stage: {3} - Can build: {4} - next stage will cost: {1} - reward: {2}\n".format(player.wonder[0], CheckWonderResource(player.stage, player.wonder), CheckWonderReward(player.stage, player.wonder), player.stage, BuildWonder(player)[0]))
    print("You currently have coins: {0}, resource: {1}\n".format(player.coin, player.supplySet))
    # based on what their current resource, it split the card into three group
    # a which is the player got all the resource to build the card without playing any cost
    handCopy = copy.copy(player.hand)
    a = CardCanBuild(player, handCopy, 0)
    for i in a:
        handCopy.remove(i)
    # b which is the player not got all the resource to build the card but can build by paying the neighbour coin
    b = CardCanBuild(player, handCopy, 1)
    for i in b:
        handCopy.remove(i[0])
    # c which is the player cannot be build
    c = CardCanBuild(player, handCopy, 2)
    hand = a+b+c
    # print to the player to let them choose
    print("Card can build:\n")
    for i in range(0, len(a)):
        print("{0}. {1}\n".format(i+1, CardPrint(hand[i])))
    print("\nCard can be build with trade\n")
    for i in range(len(a), len(a)+len(b)):
        print("{0}. {1} and will need to pay left neighbour: {2} and right neighbour: {3} for trading\n".format(i+1, CardPrint(hand[i][0]), hand[i][1][0], hand[i][1][1]))
    print("\nCard cant be build:\n")
    for i in range(len(a)+len(b), len(hand)):
        print("{0}. {1}\n".format(i+1, CardPrint(hand[i])))
    if player.buildForFree[0] is True and player.buildForFree[1] is False:
        while 1:
            print("\nDo you want to use your build for free? 1 for yes. 2 for no")
            choice = raw_input(">>> ")
            if int(choice) == 1:
                print("\nPlease enter the number of card you want to play: ")
                choice1 = raw_input(">>> ")
                card = hand.pop(int(choice1)-1)
                Update(player, card)
                player.buildForFree[1] = True
                return
            elif int(choice) == 2:
                break
            else:
                continue
    while 1:
        print("\nPlease enter the number of card you want to play: ")
        choice = raw_input(">>> ")
        copyHand = copy.copy(hand)
        card = copyHand.pop(int(choice)-1)
        # print and let the player to choose an action they want to perform
        print("\n1. Build the card\n\n2. Build wonder\n\n3. sell the card\n\nPlease enter the number of action: ")
        if int(choice)-1 <= len(hand):
            choice1 = input(">>> ")
            # perform action depend on the action and the group of the card it is in
            if int(choice1) == 1:
                if card in a:
                    print(card)
                    Update(player, card)
                    print("\nYou have built the Card: {0}".format(card[0]))
                    hand.remove(card)
                    break
                elif card in b:
                    test = Build(player, card[0], True)
                    player.coin -= test[0]
                    player.neighbour[0].coin += test[0]
                    player.coin -= test[1]
                    player.neighbour[1].coin += test[1]
                    Update(player, card[0])
                    print("\nYou have built the Card: {0}".format(card[0]))
                    hand.remove(card)
                    break
                else:
                    print("\nYou do not have enough resource to build this card")
            elif int(choice1) == 2:
                print(card)
                if BuildWonder(player)[0] is True:
                    check = BuildWonder(player)
                    if len(check) == 1:
                        UpdateWonder(player)
                        hand.remove(card)
                        print("\nYou have built the wonder")
                        break
                    else:
                        test = BuildWonder(player)
                        player.coin -= test[1][0]
                        player.neighbour[0].coin += test[1][0]
                        player.coin -= test[1][1]
                        player.neighbour[1].coin += test[1][1]
                        UpdateWonder(player)
                        print("\nYou have built the wonder")
                        hand.remove(card)
                        break
                else:
                    print("\nYou do not have enough resource to build the wonder")
            elif int(choice1) == 3:
                Discard(player, card)
                print("\nYou have discard the card")
                hand.remove(card)
                break
        else:
            continue
    copyHand = copy.copy(hand)
    # reset the card as it is back to the player hand ready to rotate
    for i in hand:
        if len(i) == 2:
            num = copyHand.index(i)
            a = copyHand.pop(num)
            b = a.pop(0)
            copyHand.append(b)
    player.hand = copyHand


# function that check what the resource of the next wonder stage
def CheckWonderResource(stage, wonders):
    if stage < wonders[2]:
        stage = 2*stage + 3
        return wonders[stage]
    else:
        return "you reached max stage number"


# function that check what the reward after build the wonder stage
def CheckWonderReward(stage, wonders):
    if stage < wonders[2]:
        stage = 2*stage + 4
        return wonders[stage]
    else:
        return "you reached max stage number"


# function that check split the card into three groups - can build, need trade and cant build
def CardCanBuild(player, hand, num):
    cardList = []
    for i in hand:
        if num == 0:
            if Build(player, i, False):
                cardList.append(i)
        elif num == 1:
            test = Build(player, i, True)
            if test is not None and test is not False:
                if len(test) == 2:
                    cardList.append([i, test])
        elif num == 2:
            if not Build(player, i, True):
                cardList.append(i)
    return cardList


# function that swap player hand
def SwapPlayerHand(lists, age):
    # if passing the hand to clockwise
    if age is 1 or age is 3:
        # loop up to the number of player
        for players in range(0, len(lists) - 1):
            # swap the first player hand with the player on the right
            lists[0].hand, lists[players + 1].hand = lists[players + 1].hand, lists[0].hand
    # if the direction is anticlockwise
    elif age is 2:
        # loop up to the number of player
        for players in range(0, len(lists)):
            # swap the last player hand with the player on the left
            lists[0].hand, lists[len(lists)-1 - players].hand = lists[len(lists)-1 - players].hand, lists[0].hand


# function that perform war
def War(lists, age):
    for player in lists:
        for i in player.neighbour:
            if player.shield > i.shield:
                print("{0} won against {1}".format(player.name, i.name))
                player.vToken[age-1] += 1
            elif player.shield < i.shield:
                print("{0} lost against {1}".format(player.name, i.name))
                player.dToken += 1
            else:
                print("{0} drew against {1}".format(player.name, i.name))
