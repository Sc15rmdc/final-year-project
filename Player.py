class Player:
    def __init__(self):
        # player hand
        self.hand = []
        # player name
        self.name = None
        # player final point
        self.fPoint = 0
        # Victory Point
        # point = [redPoint, coinPoint, wonderPoint, bluePoint, greenPoint, yellowPoint, purplePoint]
        self.point = [0, 0, 0, 0, 0, 0, 0]
        # list of the card name been built in the city
        self.card = []
        # Number of Card
        # card = [numBrown, numGrey, numPurple, numBlue, numYellow, numRed, numGreen]
        self.cardNum = [0, 0, 0, 0, 0, 0, 0]
        # neighbour
        self.neighbour = []
        # wonder
        # name of the wonder
        self.wonder = None
        # current stage of the wonder
        self.stage = 0
        # coins in the city
        self.coin = 3
        # supplySet store all the resource that can trade
        self.supplySet = []
        # wonderSet store all the resource that cant trade
        self.wonderSet = []
        # shield is the military power of the city
        self.shield = 0
        # Victory tokens = [age1, age2, age3]
        self.vToken = [0, 0, 0]
        # defeat tokens = [age1, age2, age3]
        self.dToken = 0
        # wildTech can be convert into one of any symbol at the end of game
        self.wildTech = 0
        # chains contain building that can be build for free
        self.chains = []
        # buildForFree enable player can build a card for free of their choice once per age
        # buildForFree = [Effect enable, Effect used]
        self.buildForFree = [False, False]
        # buildDiscardForFree enable the player to build a discard card at the end of the age (one time effect)
        self.buildDiscardForFree = 0
        # buildLastForFree enable the player to play the last card of the age
        self.buildLast = False
        # copy enable the player to copy the effect purple card
        self.copy = False
        # cheap trade right side
        self.cheapTradeRight = 0
        # cheap trade left side
        self.cheapTradeLeft = 0
        # cheap trade both side
        # cheapTradeBoth = [brown resource, grey resource]
        self.cheapTradeBoth = [0, 0]
        # yellow card effect to be calculate at the end of the game
        self.yEffect = []
        # number of symbol in the city
        # symbol = [gear, compass, tablet]
        self.symbol = [0, 0, 0]
        # list of effect need to be calculate at the end of the game
        self.pEffect = []
