import copy

from Action import *
from UpdateCard import *
from Heuristic import Heuristic


def GetAllPossibleAction(player, effect):
    move = []
    if effect is False:
        for i in player.hand:
            print(i)
        handCopy = copy.copy(player.hand)
        a = CardCanBuild(player, handCopy, 0)
        for i in a:
            handCopy.remove(i)
        b = CardCanBuild(player, handCopy, 1)
        for i in b:
            handCopy.remove(i[0])
        c = CardCanBuild(player, handCopy, 2)
        hand = a+b+c
        for i in hand:
            if i in a:
                move.append([i, "build"])
            elif i in b:
                move.append([i, "build/trade", i[1]])
            else:
                if player.buildForFree[0] is True and player.buildForFree[1] is False:
                    move.append([i, "build/Free"])
            test = BuildWonder(player)
            if test[0] is True:
                if len(test) == 2:
                    move.append([i, "wonder/trade", test[1]])
                else:
                    move.append([i, "wonder"])
            move.append([i, "discard"])
    else:
        for i in discard:
            move.append([i, "build"])
    return move


def UpdateMove(player, move):
    if move[1] == 'build':
        Update(player, move[0])
        player.hand.remove(move[0])
    elif move[1] == 'build/Free':
        Update(player, move[0])
        player.hand.remove(move[0])
    elif move[1] == 'build/trade':
        player.neighbour[0].coin += int(move[2][0])
        player.coin -= move[2][0]
        player.neighbour[1].coin += int(move[2][1])
        player.coin -= move[2][1]
        Update(player, move[0][0])
        if len(move[0]) == 2:
            player.hand.remove(move[0][0])
        else:
            player.hand.remove(move[0])
    elif move[1] == 'wonder':
        UpdateWonder(player)
        if len(move[0]) == 2:
            player.hand.remove(move[0][0])
        else:
            player.hand.remove(move[0])
    elif move[1] == 'wonder/trade':
        player.neighbour[0].coin += int(move[2][0])
        player.coin -= move[2][0]
        player.neighbour[1].coin += int(move[2][1])
        player.coin -= move[2][1]
        UpdateWonder(player)
        if len(move[0]) == 2:
            player.hand.remove(move[0][0])
        else:
            player.hand.remove(move[0])
    elif move[1] == 'discard':
        Discard(player, move[0])
        if len(move[0]) == 2:
            player.hand.remove(move[0][0])
        else:
            player.hand.remove(move[0])


def ChooseAIMove(player, effect):
    h = Heuristic()
    bestMove = ["" , 0]
    allMove = GetAllPossibleAction(player, effect)
    for move in allMove:
        if move[1] == "build" or move[1] == "build/Free":
            if move[0][1] == "brown":
                if bestMove[1] < h.BrownCardValue(player, move[0]):
                    bestMove[0] = move
                    bestMove[1] = h.BrownCardValue(player, move[0])
            elif move[0][1] == "grey":
                if bestMove[1] < h.GreyCardValue(player, move[0]):
                    bestMove[0] = move
                    bestMove[1] = h.GreyCardValue(player, move[0])
            elif move[0][1] == "red":
                if bestMove[1] < h.RedCardValue(player, move[0], 0):
                    bestMove[0] = move
                    bestMove[1] = h.RedCardValue(player, move[0], 0)
            elif move[0][1] == "yellow":
                if bestMove[1] < h.YellowCardValue(player, move[0], 0):
                    bestMove[0] = move
                    bestMove[1] = h.YellowCardValue(player, move[0], 0)
            elif move[0][1] == "purple":
                if bestMove[1] < h.PurpleCardValue(player, move[0], 0):
                    bestMove[0] = move
                    bestMove[1] = h.PurpleCardValue(player, move[0], 0)
            elif move[0][1] == "green":
                if bestMove[1] < h.GreenCardValue(player, move[0], 0):
                    bestMove[0] = move
                    bestMove[1] = h.GreenCardValue(player, move[0], 0)
            elif move[0][1] == "blue":
                if bestMove[1] < h.BlueCardValue(player, move[0], 0):
                    bestMove[0] = move
                    bestMove[1] = h.BlueCardValue(player, move[0], 0)
        elif move[1] == "build/trade":
            if move[0][0][1] == "red":
                if bestMove[1] < h.RedCardValue(player, move[0][0], move[2][0]+move[2][1]):
                    bestMove[0] = move
                    bestMove[1] = h.RedCardValue(player, move[0][0], move[2][0]+move[2][1])
            elif move[0][0][1] == "yellow":
                if bestMove[1] < h.YellowCardValue(player, move[0][0], move[2][0]+move[2][1]):
                    bestMove[0] = move
                    bestMove[1] = h.YellowCardValue(player, move[0][0], move[2][0]+move[2][1])
            elif move[0][0][1] == "purple":
                if bestMove[1] < h.PurpleCardValue(player, move[0][0], move[2][0]+move[2][1]):
                    bestMove[0] = move
                    bestMove[1] = h.PurpleCardValue(player, move[0][0], move[2][0]+move[2][1])
            elif move[0][0][1] == "green":
                if bestMove[1] < h.GreenCardValue(player, move[0][0], move[2][0]+move[2][1]):
                    bestMove[0] = move
                    bestMove[1] = h.GreenCardValue(player, move[0][0], move[2][0]+move[2][1])
            elif move[0][0][1] == "blue":
                if bestMove[1] < h.BlueCardValue(player, move[0][0], move[2][0]+move[2][1]):
                    bestMove[0] = move
                    bestMove[1] = h.BlueCardValue(player, move[0][0], move[2][0]+move[2][1])
        elif move[1] == "wonder":
            if bestMove[1] < h.WonderBuildValue(player, 0):
                bestMove[0] = move
                bestMove[1] = h.WonderBuildValue(player, 0)
        elif move[1] == "wonder/trade":
            if bestMove[1] < h.WonderBuildValue(player, move[2][0]+move[2][1]):
                bestMove[0] = move
                bestMove[1] = h.WonderBuildValue(player, move[2][0]+move[2][1])
        elif move[1] == "discard":
            if bestMove[1] < h.DiscardValue(player):
                bestMove[0] = move
                bestMove[1] = h.DiscardValue(player)
    print("Best Move: {0}".format(bestMove))
    UpdateMove(player, bestMove[0])






# def ChooseAIMove(player, effect):
#     allMove = GetAllPossibleAction(player, effect)
#     print(allMove)
#     num = randint(0, len(allMove)-1)
#     move = allMove.pop(num)
#     print("move: {0}".format(move))
#     UpdateMove(player, move)


# def ChooseAIMove(player, effect):
#     allMove = GetAllPossibleAction(player, effect)
#     num = randint(0, len(player.hand)-1)
#     hand = copy.copy(player.hand)
#     card = hand.pop(num)
#     for i in allMove:
#         if card == i[0]:
#             print(i[1])
#             if i[1] == "wonder" or i[1] == "wonder/trade":
#                 print("move: {0}".format(i))
#                 UpdateMove(player, i)
#                 return
#             elif i[1] == "build" or i[1] == "build/Free" or i[1] == "build/trade":
#                 print("move: {0}".format(i))
#                 UpdateMove(player, i)
#                 return
#             elif i[1] == "discard":
#                 print("move: {0}".format(i))
#                 UpdateMove(player, i)
#                 return
