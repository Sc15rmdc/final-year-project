# function that calculate final point
def CalculatePoint(player):
    # call other function
    CalculateRed(player)
    CalculateCoin(player)
    CalculateYellow(player)
    player.point[4] += CalculateGreen(player, player.wildTech)
    CalculatePurple(player)
    print("{0}'s score:".format(player.name))
    print("Red: {0}, Coin: {1}, Wonder: {2}, Blue: {3}, Yellow: {4}, Purple: {5}, Green: {6}".format(player.point[0], player.point[1], player.point[2], player.point[3], player.point[4], player.point[5], player.point[6]))
    # calculate the final point
    for i in range(0, len(player.point)-1):
        player.fPoint += player.point[i]
    return player.fPoint


# function that calculate Military point
def CalculateRed(player):
    player.point[0] += player.vToken[0] + 3 * player.vToken[1] + 5 * player.vToken[2]
    player.point[0] -= player.dToken


# function that calculate Treasury point
def CalculateCoin(player):
    player.point[1] += player.coin / 3


# function that calculate the best possible score for green
def CalculateGreen(player, wildTech):
    point = 0
    if wildTech == 0:
        point = GreenScore(player.symbol[0], player.symbol[1], player.symbol[2])
    elif wildTech == 1:
        point = max(GreenScore(player.symbol[0]+1, player.symbol[1], player.symbol[2]),
                    GreenScore(player.symbol[0], player.symbol[1]+1, player.symbol[2]),
                    GreenScore(player.symbol[0], player.symbol[1], player.symbol[2]+1))
    elif wildTech == 2:
        point = max(GreenScore(player.symbol[0]+2, player.symbol[1], player.symbol[2]),
                    GreenScore(player.symbol[0], player.symbol[1]+2, player.symbol[2]),
                    GreenScore(player.symbol[0], player.symbol[1], player.symbol[2]+2),
                    GreenScore(player.symbol[0]+1, player.symbol[1]+1, player.symbol[2]),
                    GreenScore(player.symbol[0]+1, player.symbol[1], player.symbol[2]+1),
                    GreenScore(player.symbol[0], player.symbol[1]+1, player.symbol[2]+1))
    return point


# function that calculate green point
def GreenScore(gear, compass, tablet):
    return 7*min(gear, compass, tablet) + (gear**2 + compass**2 + tablet**2)


# function that calculate yellow point
def CalculateYellow(player):
    for i in range(0, len(player.yEffect)-1):
        if player.yEffect[i] == "Haven":
            player.point[5] += player.cardNum[0]
        elif player.yEffect[i] == "Lighthouse":
            player.point[5] += player.cardNum[4]
        elif player.yEffect[i] == "Chamber of Commerce":
            player.point[5] += 2*player.cardNum[1]
        elif player.yEffect[i] == "Arena":
            player.point[5] += player.stage


# function that calculate best possible purple score
def CalculatePurple(player):
    count = 0
    for i in range(0, len(player.pEffect)):
        player.point[6] += PurpleScore(player, player.pEffect[i])
    if player.copy is True:
        for i in range(0, len(player.neighbour)):
            if player.neighbour[i].Card.count("Scientists Guild") is 1:
                temp = CalculateGreen(player, player.wildTech+1) - player.point[4]
                count = max(count, temp)
            for j in range(0, len(player.neighbour[i].pEffect)):
                count = max(count, PurpleScore(player, player.neighbour[i].pEffect[j]))
        player.point += count


# function that calculate purple score
def PurpleScore(player, effect):
    if effect == "Workers Guild":
        return player.neighbour[0].cardNum[0] + player.neighbour[1].cardNum[0]
    elif effect == "Craftsmens Guild":
        return player.neighbour[0].cardNum[1] + player.neighbour[1].cardNum[1]
    elif effect == "Traders Guild":
        return player.neighbour[0].cardNum[4] + player.neighbour[1].cardNum[4]
    elif effect == "Philosophers Guild":
        return player.neighbour[0].cardNum[6] + player.neighbour[1].cardNum[6]
    elif effect == "Spies Guild":
        return player.neighbour[0].cardNum[5] + player.neighbour[1].cardNum[5]
    elif effect == "Strategists Guild":
        return player.neighbour[0].dToken + player.neighbour[1].dToken
    elif effect == "Shipowners Guild":
        return player.cardNum[0] + player.cardNum[1] + player.cardNum[2]
    elif effect == "Magistrates Guild":
        return player.neighbour[0].cardNum[3] + player.neighbour[0].cardNum[3]
    elif effect == "Builders Guild":
        return player.stage + player.neighbour[0].stage + player.neighbour[1].stage
    else:
        return 0
