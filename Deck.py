from Card import *
import random


class Deck:
    def __init__(self):
        # card object
        self.card = Card()
        # age1 deck
        self.age1 = []
        # age2 deck
        self.age2 = []
        # age3 deck
        self.age3 = []
        # purple deck
        self.purpleCard = [self.card.workersGuild, self.card.craftsmensGuild, self.card.tradersGuild,
                           self.card.philosophersGuild, self.card.spiesGuild, self.card.strategistsGuild,
                           self.card.shipownersGuild, self.card.scientistsGuild, self.card.magistratesGuild,
                           self.card.buildersGuild]

    def AddCardToAge1Deck(self, numOfPlayer):
        if numOfPlayer >= 3:
            # Brown Card
            self.age1.append(self.card.lumberYard)
            self.age1.append(self.card.stonePit)
            self.age1.append(self.card.clayPool)
            self.age1.append(self.card.oreVein)
            self.age1.append(self.card.clayPit)
            self.age1.append(self.card.timberYard)
            # Grey Card
            self.age1.append(self.card.loom)
            self.age1.append(self.card.glassWorks)
            self.age1.append(self.card.press)
            # Blue Card
            self.age1.append(self.card.baths)
            self.age1.append(self.card.altar)
            self.age1.append(self.card.theater)
            # Yellow Card
            self.age1.append(self.card.eastTradingPost)
            self.age1.append(self.card.westTradingPost)
            self.age1.append(self.card.marketPlace)
            # Red Card
            self.age1.append(self.card.stockade)
            self.age1.append(self.card.barracks)
            self.age1.append(self.card.guardTower)
            # Green Gard
            self.age1.append(self.card.apothecary)
            self.age1.append(self.card.workshop)
            self.age1.append(self.card.scriptorium)

            if numOfPlayer >= 4:
                # Brown Card
                self.age1.append(self.card.lumberYard)
                self.age1.append(self.card.oreVein)
                self.age1.append(self.card.excavation)
                # Blue Card
                self.age1.append(self.card.pawnshop)
                # Yellow Card
                self.age1.append(self.card.tavern)
                # Red Card
                self.age1.append(self.card.guardTower)
                # Green Card
                self.age1.append(self.card.scriptorium)

                if numOfPlayer >= 5:
                    # Brown Card
                    self.age1.append(self.card.stonePit)
                    self.age1.append(self.card.clayPool)
                    self.age1.append(self.card.forestCave)
                    # Blue Card
                    self.age1.append(self.card.altar)
                    # Yellow Card
                    self.age1.append(self.card.tavern)
                    # Red Card
                    self.age1.append(self.card.barracks)
                    # Green Card
                    self.age1.append(self.card.apothecary)

                    if numOfPlayer >= 6:
                        # Brown Card
                        self.age1.append(self.card.treeFarm)
                        self.age1.append(self.card.mine)
                        # Grey Card
                        self.age1.append(self.card.loom)
                        self.age1.append(self.card.glassWorks)
                        self.age1.append(self.card.press)
                        # Blue Card
                        self.age1.append(self.card.theater)
                        # Yellow Card
                        self.age1.append(self.card.marketPlace)

                        if numOfPlayer == 7:
                            # Blue Card
                            self.age1.append(self.card.pawnshop)
                            self.age1.append(self.card.baths)
                            # Yellow Card
                            self.age1.append(self.card.tavern)
                            self.age1.append(self.card.eastTradingPost)
                            self.age1.append(self.card.westTradingPost)
                            # Red Card
                            self.age1.append(self.card.stockade)
                            # Green Card
                            self.age1.append(self.card.workshop)
            # shuffle the Age1 deck
            random.shuffle(self.age1)
            # return the shuffled Age1 deck
            return self.age1

    def AddCardToAge2Deck(self, numOfPlayer):
        if numOfPlayer >= 3:
            # Brown Card
            self.age2.append(self.card.sawmill)
            self.age2.append(self.card.quarry)
            self.age2.append(self.card.brickyard)
            self.age2.append(self.card.foundry)
            # Grey Card
            self.age2.append(self.card.loom)
            self.age2.append(self.card.glassWorks)
            self.age2.append(self.card.press)
            # Blue Card
            self.age2.append(self.card.aqueduct)
            self.age2.append(self.card.temple)
            self.age2.append(self.card.statue)
            self.age2.append(self.card.courthouse)
            # Yellow Card
            self.age2.append(self.card.forum)
            self.age2.append(self.card.caravansery)
            self.age2.append(self.card.vineyard)
            # Red Card
            self.age2.append(self.card.walls)
            self.age2.append(self.card.stables)
            self.age2.append(self.card.archeryRange)
            # Green Card
            self.age2.append(self.card.dispensary)
            self.age2.append(self.card.laboratory)
            self.age2.append(self.card.library)
            self.age2.append(self.card.school)

            if numOfPlayer >= 4:
                # Brown Card
                self.age2.append(self.card.sawmill)
                self.age2.append(self.card.quarry)
                self.age2.append(self.card.brickyard)
                self.age2.append(self.card.foundry)
                # Yellow Card
                self.age2.append(self.card.bazar)
                # Red Card
                self.age2.append(self.card.trainingGround)
                # Green Card
                self.age2.append(self.card.dispensary)

                if numOfPlayer >= 5:
                    # Grey Card
                    self.age2.append(self.card.loom)
                    self.age2.append(self.card.glassWorks)
                    self.age2.append(self.card.press)
                    # Blue Card
                    self.age2.append(self.card.courthouse)
                    # Yellow Card
                    self.age2.append(self.card.caravansery)
                    # Red Card
                    self.age2.append(self.card.stables)
                    # Green Card
                    self.age2.append(self.card.laboratory)

                    if numOfPlayer >= 6:
                        # Blue Card
                        self.age2.append(self.card.temple)
                        # Yellow Card
                        self.age2.append(self.card.forum)
                        self.age2.append(self.card.caravansery)
                        self.age2.append(self.card.vineyard)
                        # Red Card
                        self.age2.append(self.card.trainingGround)
                        self.age2.append(self.card.archeryRange)
                        # Green Card
                        self.age2.append(self.card.library)

                        if numOfPlayer >= 7:
                            # Blue Card
                            self.age2.append(self.card.aqueduct)
                            self.age2.append(self.card.statue)
                            # Yellow Card
                            self.age2.append(self.card.forum)
                            self.age2.append(self.card.bazar)
                            # Red Card
                            self.age2.append(self.card.walls)
                            self.age2.append(self.card.trainingGround)
                            # Green Card
                            self.age2.append(self.card.school)
            # Shuffle the Age2 Deck
            random.shuffle(self.age2)
            # return Shuffled Age2 Deck
            return self.age2

    def AddCardToAge3Deck(self, numOfPlayer):
        if numOfPlayer >= 3:
            # Blue Card
            self.age3.append(self.card.pantheon)
            self.age3.append(self.card.gardens)
            self.age3.append(self.card.townHall)
            self.age3.append(self.card.palace)
            self.age3.append(self.card.senate)
            # Yellow Card
            self.age3.append(self.card.haven)
            self.age3.append(self.card.lighthouse)
            self.age3.append(self.card.arena)
            # Red Card
            self.age3.append(self.card.fortifications)
            self.age3.append(self.card.arsenal)
            self.age3.append(self.card.siegeWorkshop)
            # Green Card
            self.age3.append(self.card.lodge)
            self.age3.append(self.card.observatory)
            self.age3.append(self.card.university)
            self.age3.append(self.card.academy)
            self.age3.append(self.card.study)

            if numOfPlayer >= 4:
                # Blue Card
                self.age3.append(self.card.gardens)
                # Yellow Card
                self.age3.append(self.card.haven)
                self.age3.append(self.card.chamberOfCommerce)
                # Red Card
                self.age3.append(self.card.circus)
                self.age3.append(self.card.arsenal)
                # Green Card
                self.age3.append(self.card.university)

                if numOfPlayer >= 5:
                    # Blue Card
                    self.age3.append(self.card.townHall)
                    self.age3.append(self.card.senate)
                    # Yellow Card
                    self.age3.append(self.card.arena)
                    # Red Card
                    self.age3.append(self.card.circus)
                    self.age3.append(self.card.siegeWorkshop)
                    # Green Card
                    self.age3.append(self.card.study)

                    if numOfPlayer >= 6:
                        # Blue Card
                        self.age3.append(self.card.pantheon)
                        self.age3.append(self.card.townHall)
                        # Yellow Card
                        self.age3.append(self.card.lighthouse)
                        self.age3.append(self.card.chamberOfCommerce)
                        # Red Card
                        self.age3.append(self.card.circus)
                        # Green Card
                        self.age3.append(self.card.lodge)

                        if numOfPlayer >= 7:
                            # Blue Card
                            self.age3.append(self.card.palace)
                            # Yellow Card
                            self.age3.append(self.card.arena)
                            # Red Card
                            self.age3.append(self.card.fortifications)
                            self.age3.append(self.card.arsenal)
                            # Green Card
                            self.age3.append(self.card.observatory)
                            self.age3.append(self.card.academy)
        # choosing Purple Card
        for i in range(numOfPlayer + 2):
            # add the chosen card to age3 decks
            self.age3.append(self.ChoosePurpleCard())
        # shuffle the Age3 deck
        random.shuffle(self.age3)
        # return the Age3 deck
        return self.age3

    # function that randomly choose purple card
    def ChoosePurpleCard(self):
        # generate a random number within the range of the array
        num = random.randint(0, len(self.purpleCard) - 1)
        # assign and remove the chosen card from the array
        choice = self.purpleCard.pop(num)
        # return the chosen card
        return choice

    # function that deal card to the player hand
    def DeckToPlayerHand(self, age, listOfPlayer):
        # if it is age1
        if age == 1:
            # create age1 deck
            deck1 = self.AddCardToAge1Deck(len(listOfPlayer))
            # loop up to the number of player
            for i in range(0, len(listOfPlayer)):
                # add seven cards to the player hand
                for j in range(0, 7):
                    listOfPlayer[i].hand.append(deck1.pop(0))
        # if it is age2
        elif age == 2:
            # create age2 deck
            deck2 = self.AddCardToAge2Deck(len(listOfPlayer))
            # loop up to the number of player
            for i in range(0, len(listOfPlayer)):
                # add seven cards to the player hand
                for j in range(0, 7):
                    listOfPlayer[i].hand.append(deck2.pop(0))
        # if it is age3
        elif age == 3:
            # create age3 deck
            deck3 = self.AddCardToAge3Deck(len(listOfPlayer))
            # loop up to the number of player
            for i in range(0, len(listOfPlayer)):
                # add seven cards to the player hand
                for j in range(0, 7):
                    listOfPlayer[i].hand.append(deck3.pop(0))
