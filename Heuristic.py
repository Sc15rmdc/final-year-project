from Result import PurpleScore, GreenScore, CalculateGreen
from Action import *
import copy


class Heuristic:
    def __init__(self):
        # Brown card
        self.kB = 2
        self.kBHave = 0.5
        self.kBOpp = 0.25

        # Grey card
        self.kG = 2
        self.kGHave = 2
        self.kGOpp = 1

        # Coin Worth
        self.kCoin = 0.4

        self.kBCheapTrade = 0.5
        self.kGCheapTrade = 1

        # coin used
        self.kCointrade = 0.5
        self.kBuildForFree = 5
        self.kBuildForFree = 4

        self.kGreen = 3

    def BrownCardValue(self, player, card):
        myResource = player.supplySet + player.wonderSet
        neighbourResource = player.neighbour[0].supplySet + player.neighbour[1].supplySet
        result = 0
        result1 = 0
        if card[2] == 0:
            result = self.CalculateResource(myResource, neighbourResource, card[3][0])
            return self.kB - self.kBHave * result[0] - self.kBOpp * result[1]
        elif card[2] == 1:
            if len(card[3]) == 2:
                result = self.CalculateResource(myResource, neighbourResource, card[3][0])
                return 2*(self.kB - self.kBHave * result[0] - self.kBOpp * result[1]) - self.kCointrade * 1
            else:
                if "Brick" in card[3][0]:
                    result = self.CalculateResource(myResource, neighbourResource, "brick")
                if "Stone" in card[3][0]:
                    if result == 0:
                        result = self.CalculateResource(myResource, neighbourResource, "stone")
                    else:
                        result1 = self.CalculateResource(myResource, neighbourResource, "stone")
                if "Wood" in card[3][0]:
                    if result == 0:
                        result = self.CalculateResource(myResource, neighbourResource, "wood")
                    else:
                        result1 = self.CalculateResource(myResource, neighbourResource, "wood")
                if "Ore" in card[3][0]:
                    if result == 0:
                        result = self.CalculateResource(myResource, neighbourResource, "ore")
                    else:
                        result1 = self.CalculateResource(myResource, neighbourResource, "ore")
                return self.kB - self.kBHave * result[0] - self.kBOpp * result[1] + self.kB - self.kBHave * result1[0] - self.kBOpp * result1[1]  - self.kCointrade * 1


    def GreyCardValue(self, player, card):
        myResource = player.supplySet + player.wonderSet
        neighbourResource = player.neighbour[0].supplySet + player.neighbour[1].supplySet
        if card[2][0] == "glass":
            result = self.CalculateResource(myResource, neighbourResource, "glass")
        elif card[2][0] == "paper":
            result = self.CalculateResource(myResource, neighbourResource, "paper")
        elif card[2][0] == "cloth":
            result = self.CalculateResource(myResource, neighbourResource, "cloth")
        return self.kG - self.kGHave * result[0] - self.kGOpp * result[1]


    def CalculateResource(self, myResource, neighbourResource, material):
        own = 0
        notOwn = 0
        for j in myResource:
            for i in j:
                if i == material:
                    own += 1
        for j in neighbourResource:
            for i in j:
                if i == material:
                    notOwn += 1
        return [own, notOwn]


    def RedCardValue(self, player, card , tradeAmount):
        myMilitary = player.shield
        neighbourMilitary = max(player.neighbour[0].shield, player.neighbour[1].shield)
        if card[3] == 1:
            kM = 2
        elif card[3] == 2:
            kM = 6
        elif card[3] == 3:
            kM = 10
        if myMilitary <= neighbourMilitary:
            return kM - self.kCointrade * tradeAmount
        else:
            return (kM/2) - self.kCointrade * tradeAmount


    def YellowCardValue(self, player, card, tradeAmount):
        if card[0] == "Tavern":
            return self.kCoin * 5
        elif card[0] == "East Trading Post":
            rAmount = 0
            print(player.wonder[4][0])
            if player.wonder[4][0] != "cheapTradeBothBrown":
                for i in player.neighbour[1].supplySet:
                    if "brick" in i or "stone" in i or "ore" in i or "wood" in i:
                        rAmount += 1
            return self.kBCheapTrade * rAmount
        elif card[0] == "West Trading Post":
            rAmount = 0
            print(player.wonder[4][0])
            if player.wonder[4][0] != "cheapTradeBothBrown":
                for i in player.neighbour[0].supplySet:
                    if "brick" in i or "stone" in i or "ore" in i or "wood" in i:
                        rAmount += 1
            return self.kBCheapTrade * rAmount
        elif card[0] == "Market Place":
            rAmount = 0
            for i in player.neighbour[0].supplySet + player.neighbour[1].supplySet:
                if "glass" in i or "paper" in i or "cloth" in i:
                    rAmount += 1
            return self.kGCheapTrade * rAmount
        elif card[0] == "Forum":
            myResource = player.supplySet + player.wonderSet
            neighbourResource = player.neighbour[0].supplySet + player.neighbour[1].supplySet
            gResult = self.CalculateResource(myResource, neighbourResource, "glass")
            pResult = self.CalculateResource(myResource, neighbourResource, "paper")
            cResult = self.CalculateResource(myResource, neighbourResource, "cloth")
            return (self.kG - self.kGHave * gResult[0] - self.kGOpp * gResult[1]+
                    self.kG - self.kGHave * pResult[0] - self.kGOpp * pResult[1]+
                    self.kG - self.kGHave * cResult[0] - self.kGOpp * cResult[1]-
                    self.kCointrade * tradeAmount)
        elif card[0] == "Caravansery":
            myResource = player.supplySet + player.wonderSet
            neighbourResource = player.neighbour[0].supplySet + player.neighbour[1].supplySet
            wResult = self.CalculateResource(myResource, neighbourResource, "wood")
            sResult = self.CalculateResource(myResource, neighbourResource, "stone")
            bResult = self.CalculateResource(myResource, neighbourResource, "brick")
            oResult = self.CalculateResource(myResource, neighbourResource, "ore")
            return (self.kB - self.kBHave * wResult[0] - self.kBOpp * wResult[1]+
                    self.kB - self.kBHave * sResult[0] - self.kBOpp * sResult[1]+
                    self.kB - self.kBHave * bResult[0] - self.kBOpp * bResult[1]+
                    self.kB - self.kBHave * oResult[0] - self.kBOpp * oResult[1]-
                    self.kCointrade * tradeAmount)
        else:
            CopyPlayer = copy.copy(player)
            Update(CopyPlayer, card)
            coin = CopyPlayer.coin - player.coin
            point = CopyPlayer.point[5] - player.point[5]
            return point + self.kCoin * coin - self.kCointrade * tradeAmount


    def PurpleCardValue(self, player, card, tradeAmount):
        if card[0] != "Scientists Guild":
            point = PurpleScore(player, card[3])
        else:
            point = CalculateGreen(player, player.wildTech+1)
        return point - self.kCointrade * tradeAmount



    def GreenCardValue(self, player, card, tradeAmount):
        if card[3] == "compass":
            if player.symbol[1] == 0:
                point = GreenScore(player.symbol[0], player.symbol[1]+1, player.symbol[2])
            else:
                point = GreenScore(player.symbol[0], player.symbol[1]+1, player.symbol[2]) - GreenScore(player.symbol[0], player.symbol[1], player.symbol[2])
        elif card[3] == "gear":
            if player.symbol[0] == 0:
                point = GreenScore(player.symbol[0]+1, player.symbol[1], player.symbol[2])
            else:
                point = GreenScore(player.symbol[0]+1, player.symbol[1], player.symbol[2]) - GreenScore(player.symbol[0], player.symbol[1], player.symbol[2])
        elif card[3] == "tablet":
            if player.symbol[2] == 0:
                point = GreenScore(player.symbol[0], player.symbol[1], player.symbol[2]+1)
            else:
                point = GreenScore(player.symbol[0], player.symbol[1], player.symbol[2]+1) - GreenScore(player.symbol[0], player.symbol[1], player.symbol[2])
        if player.symbol[2] == 0 or player.symbol[0] == 0 or player.symbol[1] == 0:
            return point * self.kGreen - self.kCointrade * tradeAmount
        else:
            return point - self.kCointrade * tradeAmount


    def BlueCardValue(self, player, card, tradeAmount):
        return card[3] - self.kCointrade * tradeAmount

    def DiscardValue(self, player):
        return self.kCoin * 3


    def WonderBuildValue(self, player, tradeAmount):
        reward = 0
        value = 0
        if player.stage == 0:
            reward = 4
        elif player.stage == 1:
            reward = 6
        elif player.stage == 2:
            reward = 8
        elif player.stage == 3:
            reward = 10
        for i in range(0, len(player.wonder[reward])):
            if player.wonder[reward][i][0] == "point":
                value += player.wonder[reward][i][1]
            elif player.wonder[reward][i][0] == "shield":
                if player.wonder[reward][i][1] == 1:
                    value += 2
                elif player.wonder[reward][i][1] == 2:
                    value += 6
            elif player.wonder[reward][i][0] == "coin":
                value += self.kCoin * player.wonder[reward][i][1]
            elif player.wonder[reward][i][0] == "buildDiscardForFree" or player.wonder[reward][i]:
                value += len(discard)
            elif player.wonder[reward][i] == "wildBrown":
                myResource = player.supplySet + player.wonderSet
                neighbourResource = player.neighbour[0].supplySet + player.neighbour[1].supplySet
                wResult = self.CalculateResource(myResource, neighbourResource, "wood")
                sResult = self.CalculateResource(myResource, neighbourResource, "stone")
                bResult = self.CalculateResource(myResource, neighbourResource, "brick")
                oResult = self.CalculateResource(myResource, neighbourResource, "ore")
                value += (self.kB - self.kBHave * wResult[0] - self.kBOpp * wResult[1]+
                          self.kB - self.kBHave * sResult[0] - self.kBOpp * sResult[1]+
                          self.kB - self.kBHave * bResult[0] - self.kBOpp * bResult[1]+
                          self.kB - self.kBHave * oResult[0] - self.kBOpp * oResult[1])
            elif player.wonder[reward][i] == "cheapTradeBothBrown":
                rAmount = 0
                for i in player.neighbour[0].supplySet + player.neighbour[1].supplySet:
                    if "brick" in i or "stone" in i or "ore" in i or "wood" in i:
                        rAmount += 1
                value += self.kBCheapTrade * rAmount
            elif player.wonder[reward][i] == "wildTech":
                value += CalculateGreen(player, player.wildTech+1) - CalculateGreen(player, wildTech)
            elif player.wonder[reward][i] == "buildForFree":
                value += self.kBuildForFree
            elif player.wonder[reward][i] == "wildGrey":
                myResource = player.supplySet + player.wonderSet
                neighbourResource = player.neighbour[0].supplySet + player.neighbour[1].supplySet
                gResult = self.CalculateResource(myResource, neighbourResource, "glass")
                pResult = self.CalculateResource(myResource, neighbourResource, "paper")
                cResult = self.CalculateResource(myResource, neighbourResource, "cloth")
                value += (self.kG - self.kGHave * gResult[0] - self.kGOpp * gResult[1]+
                          self.kG - self.kGHave * pResult[0] - self.kGOpp * pResult[1]+
                          self.kG - self.kGHave * cResult[0] - self.kGOpp * cResult[1])
            elif player.wonder[reward][i] == "copy":
                count = 0
                for i in range(0, len(player.neighbour)):
                    if player.neighbour[i].Card.count("Scientists Guild") is 1:
                        temp = CalculateGreen(player, player.wildTech+1) - player.point[4]
                        count = max(count, temp)
                    for j in range(0, len(player.neighbour[i].pEffect)):
                        count = max(count, PurpleScore(player, player.neighbour[i].pEffect[j]))
                value += count
            elif player.wonder[reward][i] == "buildLastForFree":
                value += self.kBuildLast
        return value - self.kCointrade * tradeAmount
